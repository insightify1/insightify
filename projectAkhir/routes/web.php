<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PertanyaanController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\JawabanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function () {
    Route::get('/', function () {
        return view('index');
    });
    
    Route::get('/dashboard', function () {
        return view('index');
    });
    
    Route::resource('category', CategoryController::class);


    Route::resource('pertanyaan', PertanyaanController::class);


    route::get('/error', function() {
        return view('error');
    });

    //Route Update Profile
    Route::get('/profile', [ProfileController::class, 'index']);
    Route::put('/profile/{id}', [ProfileController::class, 'update']);

    Route::post('/jawaban/{pertanyaan_id}', [JawabanController::class, 'store']);
    Route::get('jawaban/{id}/edit', [JawabanController::class, 'edit']);
    Route::put('jawaban/{id}', [JawabanController::class, 'update']);
    Route::delete('jawaban/{id}', [JawabanController::class, 'destroy']);

});

Auth::routes();


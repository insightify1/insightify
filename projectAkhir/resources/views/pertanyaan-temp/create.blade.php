@extends('layouts.master')

@section('title', 'Add Question')

@section('content')
    <form action="/pertanyaan" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label>Pertanyaan</label>
            <input type="text" name="content" class="form-control @error('content') is-invalid @enderror">
            @error('content')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label>Gambar</label>
            <div class="custom-file">
                <input type="file" class="custom-file-input @error('gambar') is-invalid @enderror" id="inputGroupFile01"
                    name="gambar">
                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
            </div>
            @error('gambar')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label>Category</label>
            <select name="category_id" class="form-control">
                <option value="">--Pilih Kategori--</option>
                @forelse ($category as $item)
                    <option value="{{ $item->id }}">{{$item->nama}}</option>
                @empty
                    <option value="">Tidak ada kategori</option>
                @endforelse
            </select>
            @error('content')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection

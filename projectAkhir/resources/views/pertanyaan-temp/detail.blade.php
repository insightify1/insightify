@extends('layouts.master')

@section('title', 'About Question')

@section('content')
    <div class="card">
        <div class="card-body">
            <h5 class="card-title text-info"><small class="text-secondary ml-2">{{ $pertanyaan->user->name }} | {{ $pertanyaan->updated_at->format('l, d F Y ') }}</small></h5>
            
            <h3 class="card-text">{{ $pertanyaan->content }}</h3>
        </div>
        <img class="card-img-top rounded mx-auto img-fluid" src="{{ asset('gambar/' . $pertanyaan->gambar) }}"
            alt="Card image cap" style="max-width: 30%; height: 25%;">
        <div class="border-bottom mt-5"></div>
        <br>
    </div>

    @forelse ($pertanyaan->jawaban as $respon)
        <div class="card">
            <div class="card-header">
                {{ $respon->user->name }}
            </div>
            <div class="card-body">
                <h5 class="card-text">{{ $respon->content }}</h5>
                <div class="d-flex">
                    <a href="/jawaban/{{ $respon->id }}/edit" class="btn btn-info btn-sm mt-5">Edit</a>
                    <form action="/jawaban/{{ $respon->id }}" method="post">
                        @csrf


                        @method('delete')
                        <button type="submit" class="btn btn-danger btn-sm mt-5 ml-2">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    @empty
        <br>
        <h5 class="text-primary">Answer now! you will be the first to answer it</h5>
    @endforelse
    <hr>
    <form action="/jawaban/{{ $pertanyaan->id }}" method="post">
        @csrf
        <div class="form-group">
            <textarea name="content" id="" cols="30" rows="10"
                class="form-control @error('content') is-invalid @enderror" placeholder="Add Answer"></textarea>
            @error('content')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Add Answer</button>
    </form>
@endsection

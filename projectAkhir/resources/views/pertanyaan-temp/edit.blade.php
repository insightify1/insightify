@extends('layouts.master')

@section('title', 'Edit Question')

@section('content')
    <form action="/pertanyaan/{{$pertanyaan->id}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Pertanyaan</label>
            <input type="text" name="content" value="{{$pertanyaan->content}}" class="form-control @error('content') is-invalid @enderror">
            @error('content')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label>Gambar</label>
            <div class="custom-file">
                <input type="file" class="custom-file-input @error('gambar') is-invalid @enderror" id="inputGroupFile01"
                    name="gambar">
                <label class="custom-file-label" for="inputGroupFile01">{{$pertanyaan->gambar}}</label>
            </div>
            @error('gambar')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label>Category</label>
            <select name="category_id" class="form-control">
                <option value="">--Pilih Kategori--</option>
                @forelse ($category as $item)
                    @if ($item->id === $pertanyaan->category_id)
                    <option value="{{ $item->id }}" selected>{{ $item->nama }}</option>
                    @else
                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                    @endif
                @empty
                    <option value="">Tidak ada kategori</option>
                @endforelse
            </select>
            @error('content')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection

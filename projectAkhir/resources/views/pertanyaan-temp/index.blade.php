@extends('layouts.master')

@section('title', 'Question')

@section('button')
   <a href="/pertanyaan/create"><button class="btn btn-sm btn-primary" type="button">Add Question</button></a> 
@endsection

@section('content')
    @forelse ($pertanyaan as $item)
        <div class="card">
            <div class="card-body">
                <span class="badge badge-info float-right">{{$item->category->nama}}</span>
                <h5 class="card-title text-info">{{ $item->user->name }} <small
                        class="text-secondary ml-2">{{ $item->updated_at }}</small></h5>
                <h5 class="card-text">{{ Str::limit($item->content, 100)}}</h5>
            </div>
            <img class="card-img-top rounded mx-auto img-fluid" src="{{ asset('gambar/' . $item->gambar) }}"
                alt="Card image cap" style="max-width: 30%; height: 25%;">
            <div class="card-body d-flex justify-content-center">
                <a href="/pertanyaan/{{ $item->id }}" class="btn btn-primary btn-sm mr-2">Detail and Answer</a>
                <a href="/pertanyaan/{{ $item->id }}/edit" class="btn btn-dark btn-sm mr-2">Edit</a>
                <form action="/pertanyaan/{{ $item->id }}" method="post">
                    @csrf
                    @method('delete')
                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                </form>
            </div>
            <div class="border-bottom mt-5"></div>
        </div>
    @empty
    @endforelse
@endsection

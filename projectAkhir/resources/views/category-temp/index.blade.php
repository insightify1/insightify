@extends('layouts.master')

@section('title', 'Category')

@section('button')
   <a href="/category/create"><button class="btn btn-sm btn-primary" type="button">Add Category</button></a> 
@endsection

@section('content')
    <table class="table">
        <thead class="thead">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama Category</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($category as $key=> $item)
                <tr>
                    <th scope="row">{{ $key + 1 }}</th>
                    <td>{{ $item->nama }}</td>
                    <td class="d-flex">
                        <a href="/category/{{$item->id}}" class="btn btn-info btn-sm mr-2">Detail</a>
                        <a href="/category/{{$item->id}}/edit" class="btn btn-warning btn-sm mr-2">Edit</a>
                        <form action="/category/{{$item->id}}" method="post">
                            @method('delete')
                            @csrf
                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                        </form>
                    </td>
                </tr>
            @empty
            @endforelse
        </tbody>
    </table>
@endsection

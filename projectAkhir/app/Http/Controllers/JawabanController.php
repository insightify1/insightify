<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Jawaban;
use Auth;
use Alert;


class JawabanController extends Controller
{
    public function store(Request $request, $pertanyaan_id) {
        $request->validate([
            'content' => 'required',
        ]);

        $userId = Auth::id();

        $jawaban = new Jawaban;

        $jawaban->pertanyaan_id = $pertanyaan_id;
        $jawaban->user_id = $userId;
        $jawaban->content = $request['content'];

        $jawaban->save();

        Alert::success('Tambah Jawaban','Berhasil menambahkan jawaban baru');
        return redirect('/pertanyaan/'. $pertanyaan_id);
    }

    public function edit($id) {
        $jawaban = Jawaban::find($id);

        if (Auth::id() === $jawaban->user_id) {
            return view('jawaban-temp.edit', ['jawaban'=>$jawaban]);
        } else {
            return redirect('/error');
        }
    }

    public function update(Request $request, $id) {
        $request->validate([
            'content' => 'required',
        ]);
 
        $jawaban = Jawaban::find($id);
        $pertanyaan_id = $jawaban['pertanyaan_id'];

        $jawaban->content = $request['content'];

        $jawaban->save();

        Alert::success('Ubah Jawaban','Berhasil mengubah jawaban');
        return redirect('/pertanyaan/'. $pertanyaan_id);
    }

    public function destroy($id) {
        $jawaban = Jawaban::find($id);
        $pertanyaan_id = $jawaban['pertanyaan_id'];

        $userId = $jawaban['user_id'];

        if (Auth::id() === $userId) 
        {
            $jawaban->delete();
            Alert::toast('Jawaban berhasil dihapus','success');
            return redirect('/pertanyaan/'. $pertanyaan_id);

        } else 
        {
            return redirect('/error');
        }
    }
}

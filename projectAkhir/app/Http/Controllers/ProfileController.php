<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Profile;
use Alert;

class ProfileController extends Controller
{
    public function index(){
        $userId = Auth::id();

        $profile = Profile::where('user_id', $userId)->first();

        return view('profile-temp.edit', ['profile' => $profile ]);
    }

    public function update(Request $request, $id){
        $request->validate([
            'umur' => 'required|integer',
            'bio' => 'required',
            'alamat' => 'required'
        ]);

        $profile = Profile::find($id);

        $profile->umur = $request->input('umur');
        $profile->bio = $request->input('bio');
        $profile->alamat = $request->input('alamat');

        $profile->save();
        
        Alert::success('Ubah Profile', 'Selamat profil anda sudah berhasil diubah');
        return redirect('/profile');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{
    use HasFactory;

    use HasFactory;

    protected $table = 'jawaban';

    protected $fillable = ['pertanyaan_id', 'user_id', 'content'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    }

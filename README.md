# Insightify
SELAMAT DATANG DI INSIGHTIFY
Insightify adalah platform diskusi online yang memungkinkan pengguna untuk berpartisipasi dalam forum tanya jawab. Di sini, Anda dapat berbagi pengetahuan, bertanya, dan menjawab pertanyaan sesuai minat dan bidang keahlian Anda.

Bagaimana Cara Menggunakan Insightify?
Registrasi dan Login: Jika Anda belum memiliki akun, daftarlah dengan mengisi formulir pendaftaran. Jika sudah memiliki akun, cukup masuk menggunakan informasi login Anda.

Jelajahi Kategori: Setelah masuk, Anda dapat menjelajahi berbagai kategori pertanyaan yang tersedia di forum. Pilih kategori yang  sesuai kebutuhan Anda.

Bertanya: Jika Anda memiliki pertanyaan, buatlah posting pertanyaan baru. Tuliskan pertanyaan Anda dengan jelas dan rinci.

Menjawab Pertanyaan: Anda juga dapat menjawab pertanyaan yang diajukan oleh pengguna lain. Berikan jawaban yang informatif dan membantu.

Profil Pengguna: Anda dapat mengatur profil pengguna Anda dengan menambahkan informasi umur, alamat dan biografi.

Panduan Etika: Tetaplah berbicara dengan sopan dan menghormati anggota lain dalam forum. Jangan ragu untuk memberikan nilai tambah dan dukungan positif.

Kontribusi
Insightify Forum merupakan tempat untuk berbagi pengetahuan dan pengalaman. Kami mengundang Anda untuk turut berpartisipasi dengan bertanya, menjawab, dan berinteraksi dengan anggota lain. Mari bersama-sama membangun komunitas yang bermanfaat dan mendukung.

KELOMPOK 4
1. Timothy Erlangga ( tmothyerlangga )
2. Ahmad Haykal Yunus (@haykalyns)
3. Muhammad Bachtiar ( @muhammaddbachtiar )

Tema Project : Forum Tanya Jawab (INSIGHTIFY)

Link Demo : https://drive.google.com/file/d/1GBCuMpWGo5L121DPY0OrqdcVRgG241oV/view?usp=sharing
